var botaoAdcionar = document.querySelector("#adicionar-paciente");

botaoAdcionar.addEventListener("click", function(event) {
    event.preventDefault();
    var form = document.querySelector("#form-adciona");

    var paciente  = obtemPacienteDoFormulario (form)
    var erros = validaPaciente(paciente);

    if(erros.length > 0){
        exibeMensagemDeErro(erros);
        return;
    }

    adionaPacienteNaTabela(paciente)
    form.reset();
    var mensagemDeErro = document.querySelector("#mensagem-erro");
    mensagemDeErro.innerHTML = ""
});


function obtemPacienteDoFormulario(form) {
    var paciente = {
        nome: form.nome.value,
        altura: form.altura.value,
        peso: form.peso.value,
        gordura: form.gordura.value,
        imc: calculaImc(form.peso.value, form.altura.value)
    }
    return paciente
}

function montaTr(paciente) {
    var pacienteTr = document.createElement("tr")
    pacienteTr.classList.add("paciente");

    pacienteTr.appendChild(montaTd(paciente.nome, "info-nome"));
    pacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
    pacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
    pacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura"));
    pacienteTr.appendChild(montaTd(paciente.imc, "info-imc"));

    return pacienteTr;
}

function montaTd(dado, classe) {
    var td = document.createElement("td");
    td.textContent = dado;
    td.classList.add(classe);
    return td;
}


function validaPaciente(paciente) {
    
    var erros = [];
    if(!validaPeso(paciente.peso)) {
        erros.push("O peso é invalido");
    }
    if (!validaPeso(paciente.altura)) {
        erros.push("O altura é invalido");
    }
    if( paciente.nome.length == 0) {
        erros.push("O nome não pode ser em branco");
    }
    if(paciente.gordura.length == 0) {
        erros.push("A gordura não pode ser em branco");
    }
    if(paciente.peso.length == 0) {
        erros.push("A peso não pode ser em branco");
    }
    if(paciente.altura.length == 0) {
        erros.push("A altura não pode ser em branco");
    }

    return erros;
}

function exibeMensagemDeErro(erros) {
    var ul = document.querySelector("#mensagem-erro");
    ul.innerHTML = "";
    erros.forEach(function(erro) {
        var li = document.createElement("li");
        li.textContent = erro;
        ul.appendChild(li);
    });
}

function adionaPacienteNaTabela(paciente) {
    var pacienteTr = montaTr(paciente);
    var tabela = document.querySelector("#tabela-pacientes");
    tabela.appendChild(pacienteTr);
}