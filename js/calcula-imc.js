var titulo = document.querySelector(".titulo");
titulo.textContent = "Aparecida Nutricionista";


var pacientes = document.querySelectorAll(".paciente");
for (var i = 0; i < pacientes.length; i++){

    var paciente = pacientes[i]
    var tdPeso = paciente.querySelector(".info-peso");
    var peso = tdPeso.textContent;
    var tdAltura = paciente.querySelector(".info-altura");
    var altura = tdAltura.textContent;

    var pesoEhValido = validaPeso(peso);
    var alturaEhValido = validaAltura(altura);
    var tdImc = paciente.querySelector(".info-imc");

    if(!pesoEhValido){
        console.log("peso invalido");
        pesoEhValido = 
        tdImc.textContent = "Peso invalido";
        paciente.classList.add("paciente-invalido")
    }
    if(!alturaEhValido){
        console.log("altura invalido");
        alturaEhValido = false;
        tdImc.textContent = "Altura invalido";
        paciente.classList.add("paciente-invalido")
    }

    if(pesoEhValido && alturaEhValido){
        var imc = calculaImc(peso, altura);
        tdImc.textContent = imc;
    }
}

titulo.addEventListener("click", function() {
    console.log("olha so posso chamar uma funcao anonima")
});

// function mostraMensagem() {
    //     console.log("ola eu fui clicado");
    // }
    
function calculaImc(peso, altura) {
    var imc = 0;
    imc = peso / (altura * altura);

    return imc.toFixed(2);
}

function validaPeso(peso) {
    if(peso >= 0 && peso < 1000){
        return true;
    } else {
        return false;
    }
}

function validaAltura(altura) {
    if(altura >= 0 && altura < 1000){
        return true;
    } else {
        return false;
    }
}